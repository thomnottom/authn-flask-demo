from pingfederate import PingFederateOAuth
from getpass import getpass

CLIENT_ID = "wtcapp2"
CLIENT_SECRET = "secret"

pingfederate_oauth = PingFederateOAuth(CLIENT_ID, CLIENT_SECRET)


def create_menu(options):
    option_list = list(options)
    option_number = 1
    for option in option_list:
        print(f"{option_number}: {option}")
        option_number += 1

    choice = int(input("What do you want to do? "))
    return option_list[choice - 1]


def mfa_choice(options):
    option_list = list(options)
    option_number = 1
    for option in option_list:

        print(f"{option_number}: {option['target']} ({option['type']})")
        option_number += 1

    choice = int(input("What do you want to do? "))
    return option_list[choice - 1]


def login():
    username = input("Username: ")
    password = getpass(prompt="Password: ")
    credentials = {
        'username': username,
        'password': password
    }
    return credentials


def main():

    pingfederate_oauth.initiate_oauth_flow()
    
    flow_choice = create_menu(pingfederate_oauth.paths)
    if flow_choice == "checkUsernamePassword":
        credentials = login()
        pingfederate_oauth.check_username_password(credentials['username'], credentials['password'])
        print(pingfederate_oauth.status)
    else:
        print(f"{flow_choice} not yet implemented.")

    devices = pingfederate_oauth.mfa_authenticate()

    pingfederate_oauth.mfa_select_device()
    flow_choice = create_menu(pingfederate_oauth.paths)
    


while input("Do you want to authenticate? ") == "y":
    main()

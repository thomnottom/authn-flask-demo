from requests import Request, Session
from icecream import ic
import json
import uuid


OAUTH_CLIENTS = {
    'retail_dev': {
        'name': 'Retail Local DEV Client',
        'client_id': 'retail-app.localdev'
    },
    'wtc_dev': {
        'name': 'WTC Local DEV Client',
        'client_id': 'wtc-app.localdev'
    }
}


def pretty_print_POST(req):
    print('{}\n{}\r\n{}\r\n\r\n{}'.format(
        '-----------START-----------',
        req.method + ' ' + req.url,
        '\r\n'.join('{}: {}'.format(k, v) for k, v in req.headers.items()),
        req.body,
    ))


class PingFederateOAuth:

    def __init__(self, pf_environ, oauth_client):
        self._request = {}
        self._response = {}
        #self.base_url = pf_environ['base_url']
        self.wellknown = self._get_wellknown(pf_environ['base_url'])
        self.client_id = OAUTH_CLIENTS[oauth_client]['client_id']
        self.headers = {
            'X-XSRF-Header': 'pf-authn-flask-demo',
            'Content-Type': 'application/json'
        }
        self.session = ""
        self.flowid = ""
        self.status = ""
        self.error_code = ""
        self.error_details = ""
        self.paths = {}
        self.devices = ""
        self.code = ""
        self.pm_data = ""
        self.state = ""
        self.token = {
            'access_token': '',
            'expires_in': '',
            'id_token': '',
            'refresh_token': '',
            'token_type': ''
        }

    # Pull all info from Well Known endpoint
    def _get_wellknown(self, base_url):
        url = f'{base_url}.well-known/openid-configuration'
        s = Session()
        req = Request('POST', url)
        prepped = req.prepare()
        resp = s.send(prepped)
        return resp.json()

    def _set_status(self, response_data):
        request_data = response_data.request
        json_data = response_data.json()
        ic(json_data)
        flowid = json_data.get('id')
        if flowid and not (flowid == self.flowid):
            self.flowid = flowid

        if not request_data.body:
            request_body = ''
        else:
            request_body = json.loads(request_data.body)
        self._request = (
            '{}\r\n{}\r\n\r\n{}'.format(
                request_data.method + ' ' + request_data.url,
                '\r\n'.join('{}: {}'.format(k, v)
                            for k, v in request_data.headers.items()),
                request_body,
            )
        )
        self._response = (
            '{}\r\n{}\r\n\r\n{}'.format(
                str(response_data.status_code) + ' ' + response_data.reason,
                '\r\n'.join('{}: {}'.format(k, v)
                            for k, v in response_data.headers.items()),
                json.dumps(json_data, indent=4, separators=(',', ': ')),
            )
        )
        if "status" in json_data:
            self.status = json_data["status"]
            if self.status == "COMPLETED":
                self.code = json_data['authorizeResponse']['code']
            self.paths = json_data["_links"]
            self.error_code = ""
            self.error_details = ""
        elif "code" in json_data:
            self.error_code = json_data["code"]
            self.error_details = json_data["details"]
        else:
            pass
        if 'devices' in json_data:
            self.devices = json_data['devices']
        if 'pmDataValue' in json_data:
            ic(json_data['pmDataValue'])
            self.pm_data = json_data['pmDataValue']

    def _send_api_request(self, request):
        prepared_request = self.session.prepare_request(request)
        pretty_print_POST(prepared_request)
        response = self.session.send(prepared_request)
        self._set_status(response)

    def _prepare_api_request(self, call, payload):
        ic(payload)
        url = f"{self.paths[call]['href']}?action={call}"
        request = Request("POST", url, headers=self.headers, json=payload)
        self._send_api_request(request)

    def _send_oauth_request(self, request):
        prepared_request = self.session.prepare_request(request)
        pretty_print_POST(prepared_request)
        response = self.session.send(prepared_request)
        self.token = response.json()

    def _prepare_oauth_request(self, endpoint, headers, payload):
        ic(payload)
        url = f"{self.base_url}{endpoint}"
        request = Request("POST", url, headers=headers, data=payload)
        self._send_oauth_request(request)

    def initiate_oauth_flow(self):
        self.session = Session()
        self.state = uuid.uuid4()
        payload = {}
        headers = {
            'X-XSRF-Header': 'pf-authn-flask-demo',
            'Content-Type': 'application/vnd.pingidentity.initiateOAuthAuthorization+json'
        }
        url = (f"{self.wellknown['authorization_endpoint']}?response_type=code&"
                f"client_id={self.client_id}&"
                f"scope=openid&"
                f"response_mode=pi.flow&"
                f"state={self.state}")
        request = Request("POST", url, headers=headers, data=payload)
        self._send_api_request(request)

    def send_headers(self, capture_headers):
        header_results = []
        for key, value in capture_headers.items():
            header_results.append({"headerName": key, "headerValue": value})
        payload = {
            "headers": header_results
        }
        self._prepare_api_request(call='submitHttpHeaders', payload=payload)

    def check_username_password(self, username, password):
        payload = {
            "username": username,
            "password": password
        }
        self._prepare_api_request(
            call='checkUsernamePassword', payload=payload)

    def efl_analyze(self, device_fingerprint, ip_address, user_agent):
        payload = {
            "deviceFingerprint": device_fingerprint,
            # "ipAddress": "54.93.127.20",  # ip_address,
            "userAgent": user_agent,
            "deviceTokenCookie": "None"
        }
        self._prepare_api_request(call='submitFraudChallenge', payload=payload)

    def mfa_initiate(self):
        payload = {
        }
        self._prepare_api_request(call='authenticate', payload=payload)

    def mfa_device_select(self, device_id):
        payload = {
            "deviceRef": {
                "id": device_id
            }
        }
        self._prepare_api_request(call='selectDevice', payload=payload)

    def mfa_check_otp(self, otp_response):
        payload = {
            "otp": otp_response
        }
        self._prepare_api_request(call='checkOtp', payload=payload)

    def continue_authentication(self):
        payload = {
        }
        self._prepare_api_request(
            call='continueAuthentication', payload=payload)

    def submit_my_device(self, my_device=True):
        payload = {
            "myDevice": my_device
        }
        self._prepare_api_request(call='submitMyDevice', payload=payload)

    def api_call_no_payload(self, callname):
        payload = {
        }
        self._prepare_api_request(
            call=callname, payload=payload)

    def exchange_token(self):
        auth_headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        auth_payload = {
            'client_id': self.client_id,
            'grant_type': 'authorization_code',
            'response_type': 'code',
            'code': self.code
        }
        self._prepare_oauth_request(
            endpoint=self.wellknown['token_endpoint'], headers=auth_headers, payload=auth_payload)

    def initiate_step_up(self):
        payload = {}
        headers = {
            'X-XSRF-Header': 'pf-authn-flask-demo',
            'Content-Type': 'application/vnd.pingidentity.initiateOAuthAuthorization+json'
        }
        url = (f"{self.wellknown['authorization_endpoint']}?response_type=code&"
                f"client_id={self.client_id}&"
                f"scope=openid&"
                f"response_mode=pi.flow&"
                f"state={self.state}&"
                f"acr_values=WTCstepUp")
        request = Request("POST", url, headers=headers, data=payload)
        self._send_api_request(request)

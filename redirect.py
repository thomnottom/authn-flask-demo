from flask import Flask
from flask_oauthlib.client import OAuth

app = Flask(__name__)
app.secret_key = "This is my secret key..."
bootstrap = Bootstrap(app)

pf_oauth_redirect = OAuth.remote_app(name='pfd-ciam-dev',
    consumer_key='wtc-app.localdev',
    #consumer_secret='92b7cf30bc42c49d589a10372c3f9ff3bb310037',
    request_token_params={'scope': 'openid'},
    base_url='https://pfd-ciam-dev.mtb.com/',
    request_token_url='https://pfd-ciam-dev.mtb.com/as/token.oauth2',
    access_token_method='POST',
    access_token_url='https://pfd-ciam-dev.mtb.com/as/introspect.oauth2',
    authorize_url='https://pfd-ciam-dev.mtb.com/as/authorization.oauth2'
)


@app.route('/')
def home():
    return render_template('index.html', flow_data=pf_oauth_api)


@app.route('/login')
def login():
    return pf_oauth_redirect.authorize(callback=url_for('oauth_authorized',
        next=request.args.get('next') or request.referrer or None))


@app.route('/oauth-authorized')
def oauth_authorized():
    next_url = request.args.get('next') or url_for('index')
    resp = pf_oauth_redirect.authorized_response()
    if resp is None:
        flash(u'You denied the request to sign in.')
        return redirect(next_url)

    session['pfd-ciam-dev_token'] = (
        resp['oauth_token']
    )
    session['pfd-ciam-dev_token'] = resp['username']

    flash('You were signed in as %s' % resp['username'])
    return redirect(next_url)


if __name__ == '__main__':
    app.run(debug=True)

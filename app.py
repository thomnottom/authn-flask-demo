from environs import Env
from flask import Flask, render_template, redirect, url_for, request
from flask.helpers import make_response
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, RadioField, validators
from wtforms.fields.simple import HiddenField
from wtforms.validators import DataRequired
from pingfederate import PingFederateOAuth
from icecream import ic
import json

# Read variables from environment file (.env)
env = Env()
env.read_env()

# Get PingFederate environments from config file (config.json)
f = open('config.json')
app_config = json.load(f)
# Load available PingFederate environments. For now, hard code CIAM DEV
pf_environs = app_config['pf_environments']
pf_active_env = pf_environs['pfd-ciam-dev']

# Declare the OAuth object - this configures the object, but the session
# still needs to be initiated. 
pf_oauth_api = PingFederateOAuth(pf_active_env, 'wtc_dev')

# Flask app app declaration
app = Flask(__name__)
app.secret_key = env("APP_SECRET")
bootstrap = Bootstrap(app)


@app.route('/')
def home():
    return render_template('index.html', flow_data=pf_oauth_api)


@app.route('/authn_api', methods=['GET', 'POST'])
def authn_api():
    # Currently implemented PingFed API status codes. Anything missing will 
    # lead to an error page.
    pf_policy_statuses = {
        'HTTP_HEADERS_REQUIRED': 'capture_http_headers',
        'USERNAME_PASSWORD_REQUIRED': 'html_form',
        'FRAUD_CHALLENGE_REQUIRED': 'fraud_service',
        'PMDATA_VALUE': 'fraud_service_pmdata',
        'AUTHENTICATION_REQUIRED': 'pingone_mfa',
        'DEVICE_SELECTION_REQUIRED': 'pingone_mfa_devices',
        'OTP_REQUIRED': 'pingone_mfa_otp',
        'MFA_COMPLETED': 'pingone_mfa_complete',
        'MY_DEVICE_REQUIRED': 'capture_my_device',
        'FRAUD_SERVICE_NOTIFICATION': 'fraud_notification',
        'PMDATA_UPDATE': 'fraud_notification_pmdata',
        'COMPLETED': 'authn_complete'
        }
    if pf_oauth_api.status in pf_policy_statuses:
        pf_policy_url = pf_policy_statuses[pf_oauth_api.status]
        return redirect(url_for(pf_policy_url))
    else:
        return redirect(url_for('error'))


@app.route('/initiate_authn')
def initiate_authn():
    pf_oauth_api.initiate_oauth_flow()
    return redirect(url_for('authn_api'))


@app.route('/capture_http_headers', methods=['GET', 'POST'])
def capture_http_headers():
    class CustomForm(FlaskForm):
        pass
    for key in pf_oauth_api.paths:
        if not (key == 'self'):
            setattr(CustomForm, key, SubmitField(label=key))
    form = CustomForm()
    if request.method == 'POST':
        if 'submitHttpHeaders' in request.form:
            captured_headers = request.headers
            pf_oauth_api.send_headers(captured_headers)
            return redirect(url_for('authn_api'))
    return render_template('authn_api_form.html', page_title="Capture Headers", form=form, flow_data=pf_oauth_api)


@app.route('/html_form', methods=['GET', 'POST'])
def html_form():
    class CustomForm(FlaskForm):
        pass
    setattr(CustomForm, 'login_id', StringField(
        label='Username', validators=[DataRequired()]))
    setattr(CustomForm, 'password', PasswordField(
        label='Password', validators=[DataRequired()]))
    for key in pf_oauth_api.paths:
        if not (key == 'self'):
            setattr(CustomForm, key, SubmitField(label=key))
    login_form = CustomForm()
    if request.method == 'POST':
        if login_form.validate_on_submit() and 'checkUsernamePassword' in request.form:
            pf_oauth_api.check_username_password(
                login_form.login_id.data, login_form.password.data)
            return redirect(url_for('authn_api'))
        else:
            return redirect(url_for('error'))
    return render_template('authn_api_form.html', page_title="HTML Form", form=login_form, flow_data=pf_oauth_api)


@app.route('/fraud_service', methods=['GET', 'POST'])
def fraud_service():
    class CustomForm(FlaskForm):
        pass
    setattr(CustomForm, 'device_fingerprint',
            HiddenField(label="deviceFingerprint"))
    for key in pf_oauth_api.paths:
        if not (key == 'self'):
            setattr(CustomForm, key, SubmitField(label=key))
    form = CustomForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            device_fingerprint = form.device_fingerprint.data
            ip_addr = request.remote_addr
            user_agent = request.headers['User-Agent']
            pf_oauth_api.efl_analyze(
                device_fingerprint=device_fingerprint,
                ip_address=ip_addr,
                user_agent=user_agent)
            return redirect(url_for('authn_api'))
        else:
            return redirect(url_for('error'))
    return render_template('fraud_analyze_form.html', page_title="Fraud Analyze", form=form, flow_data=pf_oauth_api)


@app.route('/fraud_service_pmdata', methods=['GET', 'POST'])
def fraud_service_pmdata():
    class CustomForm(FlaskForm):
        pass
    for key in pf_oauth_api.paths:
        if not (key == 'self'):
            setattr(CustomForm, key, SubmitField(label=key))
    form = CustomForm()
    if request.method == 'POST' and form.validate_on_submit():
        if "submitContinue" in request.form:
            pf_oauth_api.api_call_no_payload('submitContinue')
            return redirect(url_for('authn_api'))
        else:
            return redirect(url_for('error'))
    response = make_response( render_template('authn_api_form.html', page_title="Fraud Analyze - Set Cookie", form=form, flow_data=pf_oauth_api) )
    response.set_cookie( "PM_DATA", pf_oauth_api.pm_data )
    return response


@app.route('/pingone_mfa', methods=['GET', 'POST'])
def pingone_mfa():
    class CustomForm(FlaskForm):
        pass
    for key in pf_oauth_api.paths:
        if not (key == 'self'):
            setattr(CustomForm, key, SubmitField(label=key))
    form = CustomForm()
    if request.method == 'POST' and form.validate_on_submit():
        if 'authenticate' in request.form:
            pf_oauth_api.mfa_initiate()
            return redirect(url_for('authn_api'))
        elif 'cancelAuthentication' in request.form:
            pf_oauth_api.api_call_no_payload('cancelAuthentication')
            return redirect(url_for('authn_api'))
        else:
            return redirect(url_for('error'))
    return render_template('authn_api_form.html', page_title="PingOne MFA - Start", form=form, flow_data=pf_oauth_api)


@app.route('/pingone_mfa_devices', methods=['GET', 'POST'])
def pingone_mfa_devices():
    class CustomForm(FlaskForm):
        pass
    choices = []
    for device in pf_oauth_api.devices:
        choices.append(
            (device['id'], f"{device['nickname']}: {device['target']}"))
    setattr(CustomForm, 'mfa_device', RadioField(
        'MFA Device', choices=choices))
    for key in pf_oauth_api.paths:
        if not (key == 'self'):
            setattr(CustomForm, key, SubmitField(label=key))
    form = CustomForm()
    if request.method == 'POST' and 'selectDevice' in request.form:
        device_id = form.mfa_device.data
        pf_oauth_api.mfa_device_select(device_id=device_id)
        return redirect(url_for('authn_api'))
    return render_template('authn_api_form.html', page_title="PingOne MFA - Choose Device", form=form, flow_data=pf_oauth_api)


@app.route('/pingone_mfa_otp', methods=['GET', 'POST'])
def pingone_mfa_otp():
    class CustomForm(FlaskForm):
        pass
    setattr(CustomForm, 'otp_response', StringField('OTP'))
    for key in pf_oauth_api.paths:
        if not (key == 'self'):
            setattr(CustomForm, key, SubmitField(label=key))
    form = CustomForm()
    if request.method == 'POST' and 'checkOtp' in request.form:
        otp_response = form.otp_response.data
        pf_oauth_api.mfa_check_otp(otp_response=otp_response)
        return redirect(url_for('authn_api'))
    return render_template('authn_api_form.html', page_title="PingOne MFA - Check OTP", form=form, flow_data=pf_oauth_api)


@app.route('/pingone_mfa_complete', methods=['GET', 'POST'])
def pingone_mfa_complete():
    class CustomForm(FlaskForm):
        pass
    for key in pf_oauth_api.paths:
        if not (key == 'self'):
            setattr(CustomForm, key, SubmitField(label=key))
    form = CustomForm()
    if request.method == 'POST' and 'continueAuthentication' in request.form:
        ic(request.form)
        pf_oauth_api.continue_authentication()
        return redirect(url_for('authn_api'))
    return render_template('authn_api_form.html', page_title="PingOne MFA - Complete", form=form, flow_data=pf_oauth_api)

@app.route('/capture_my_device', methods=['GET', 'POST'])
def capture_my_device():
    class CustomForm(FlaskForm):
        pass
    for key in pf_oauth_api.paths:
        if not (key == 'self'):
            setattr(CustomForm, key, SubmitField(label=key))
    form = CustomForm()
    if request.method == 'POST' and 'submitMyDevice' in request.form:
        ic(request.form)
        pf_oauth_api.submit_my_device()
        return redirect(url_for('authn_api'))
    return render_template('authn_api_form.html', page_title="Capture My Device", form=form, flow_data=pf_oauth_api)


@app.route('/fraud_notification', methods=['GET', 'POST'])
def fraud_notification():
    class CustomForm(FlaskForm):
        pass
    form = CustomForm()
    if request.method == 'POST' and form.validate_on_submit():
        if "submitFraudServiceNotification" in request.form:
            pf_oauth_api.api_call_no_payload('submitFraudServiceNotification')
            return redirect(url_for('authn_api'))
        else:
            return redirect(url_for('error'))
    return render_template('authn_api_form.html', page_title="Fraud Service Notification", form=form, flow_data=pf_oauth_api)


@app.route('/fraud_notification_pmdata', methods=['GET', 'POST'])
def fraud_notification_pmdata():
    class CustomForm(FlaskForm):
        pass
    for key in pf_oauth_api.paths:
        if not (key == 'self'):
            setattr(CustomForm, key, SubmitField(label=key))
    form = CustomForm()
    if request.method == 'POST' and form.validate_on_submit():
        if "submitContinue" in request.form:
            pf_oauth_api.api_call_no_payload('submitContinue')
            return redirect(url_for('authn_api'))
        else:
            return redirect(url_for('error'))
    response = make_response( render_template('authn_api_form.html', page_title="Fraud Notify - Update Cookie", form=form, flow_data=pf_oauth_api) )
    response.set_cookie( "PM_DATA", pf_oauth_api.pm_data )
    return response


@app.route('/authn_complete', methods=['GET', 'POST'])
def authn_complete():
    class CustomForm(FlaskForm):
        submit = SubmitField(label='Exchange Code for Token')
    form = CustomForm()
    if request.method == 'POST':
        pf_oauth_api.exchange_token()
        return redirect(url_for('token'))
    return render_template('authn_api_form.html', page_title="Authn Flow Complete", form=form, flow_data=pf_oauth_api)


@app.route('/token', methods=['GET', 'POST'])
def token():
    class CustomForm(FlaskForm):
        submit = SubmitField(label="Initiate Step Up Authentication")
    form = CustomForm()
    if request.method == 'POST':
        pf_oauth_api.initiate_step_up()
        return redirect(url_for('authn_api'))
    return render_template('oauth_page.html', page_title="A TOKEN!!!", form=form, flow_data=pf_oauth_api)


@app.route('/error', methods=['GET'])
def error():
    return render_template('error.html', flow_data=pf_oauth_api)


if __name__ == '__main__':
    app.run(debug=True)

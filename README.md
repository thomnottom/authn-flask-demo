# CIAM Authn Flask Demo

The purpose of this application is to demonstrate an authentication policy on PingFederate. It is a web application built in Python/Flask designed to be run locally by engineers, but it could potentially be hosted for shared usage.


## API Authentication

The API flow allows the user to go through each step of the policy and see both the application call and PingFederate response. This visualization can also be used to troubleshoot a policy.


## OAuth/OIDC Redirect

*To be implemented*

Traditionally an OAuth/OIDC request will trigger the client to redirect to the authentication server to complete the process. This can easily be tested/demonstrated using Ping's OAuthPlayground. However, that app is limited to Java web servers and the shared installation is prone to configurations being overwritten by multiple users.

---
## Installation and Running the App


### Getting Started

If you haven't already, you follow the [Git Started Developer Setup Guide](https://git-started.mtb.com/#/Documentation/DevSetUp/devconfig) to get WSL (Windows Subsystem for Linux) installed on your VPC or Laptop. WSL may default to Python2, in which case you need to either execute the code with `python3` or change which python is the default with the `update-alternatives` command.
```
sudo update-alternatives --install /usr/bin/python python /usr/bin/python2 20
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 10
sudo update-alternatives --config python
```
Make sure that `/usr/bin/python3` is selected, or you can change it now. Once complete running `python --version` should show a version 3 release (currently 3.6.9).

To manage python libraries and environments you should install both **pip** (python library manager) and **venv** (python virtual environment manger).
```
sudo apt install python3-pip python3-venv
```

### A word on git authentication

One of the trickiest parts of git is dealing with authentication. If you are only going to run this app and not be contributing, you can always just download the entire repo as a zip file above. However it is better practice to at least clone the repo (`git clone https://gitlab.mtb.com/ciam-the-walrus/ciam-authn-flask-demo.git`) so that a simple `git pull` will update your local copy with any changes published to GitLab.

To properly authenticate to Gitlab, configure git to use the Windows Credential Manager.
```
git config --global user.name "tdpots8"
git config --global user.email "tslattery@mtb.com"
git config --global credential.helper "/mnt/c/Program\ Files/Git/mingw64/libexec/git-core/git-credential-manager.exe"
```
Now when you hit GitLab from within WSL, it will prompt you for your credentials and allow you to save them. Just make sure to open Credential Manager to update your password after it is changed.
![Windows Credential Manager](images/credential_manager.png)

### Requirements

All non-default libraries required for this app are listed in *requirements.txt*.

### Let's get to it

WSL presents its file system as a pseudo-network drive (e.g. `\\wsl$\Ubuntu-18.04\home\tdpots8\`), and it is easier to run everything within it rather than trying to connect to your Windows filesystem. If you want to open that directory in Explorer, simply run `explorer.exe .` and it will open the current directory. If you prefer to use your Windows directory structre, you can access those drives in the `/mnt` portion of the file system (e.g. `cd /mnt/f` to get to your personal network drive).

When you clone the git repo, it will automatically create the subdirectory to store it. I still prefer to create a location for keeping all of my repos.
```
mkdir code
cd code
git clone https://gitlab.mtb.com/ciam-the-walrus/ciam-authn-flask-demo.git
cd ciam-authn-flask-demo
```

*Optional: Configure virtual environment*
This is not necessary, but I prefer to use virtual environments in Python to keep libraries consistent across systems.
`python -m venv .venv`
 If you use VS Code or PyCharm, they can be configured to automatically activate the virtual environment when you open a project. On the commandline you will need to activate it manually each time.
 `source .venv/bin/activate`

 Now we need to install the required libraries. This really only needs to be done the first time, however if a new functionality requiring a different library is implemented later it will need to be re-run.
 `python -m pip install -r requirements.txt`

 Copy the file `.env_sample` to `.env` and edit the environment variables if necessary.
 `cp .env_sample .env`

 Finally flask can be run.
 `python -m flask run`

 If you see output like below, it is running properly and you can access the site at http://127.0.0.1:5000.
 ```
 (.venv) tdpots8@PCVPTWX6OP03900:~/code/ciam-authn-flask-demo$ python -m flask run
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 351-849-626
 ```

---
<a href="https://www.flaticon.com/free-icons/authentication" title="authentication icons">Authentication icons created by Eucalyp - Flaticon</a>
 
